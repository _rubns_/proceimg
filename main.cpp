#include <iostream>
#include <string>
#include "img.h"
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <limits>
#include <time.h>
//#include <windows.h>

using namespace std;

Img optRes(Img resultado, Img img,Img trabajo, bool cargada);

int main(){
    bool salir = false, cargada=false;
    char opt,op;
    int res;
    Img img,trabajo,resultado,temporal;
    string nom,canal;
    float g;
    while (!salir)
    {   system("clear");    
        cout << "Selecione una opcion:" <<endl;
        cout << "1. Negativo" <<endl;
        cout << "2. Degradado Superior" << endl;
        cout << "3. Degradado Inferior" << endl;
        cout << "4. Degradado Izquierda a Derecha" << endl;
        cout << "5. Degradado Derecha a Izquierda" << endl;
        cout << "6. Correcion Gamma" << endl;
        cout << "7. Aclarado Logaritmico" << endl;
        cout << "8. Aclarado con funcion Seno" << endl;
        cout << "9. Aclarado con funcion exponencial" << endl;
        cout << "10. Obscurecimiento con funcion exponencial" << endl;
        cout << "11. Obscurecimiento con funcion coseno" << endl;
        cout << "12. Binarizacion" << endl;
        cout << "13. Binarizacion por canal" << endl;
        cout << "14. Histograma escala de grises" << endl;
        cout << "15. Grayscale" << endl;
        cout << "0. Salir" << endl;
        cout << "Respuesta:";
        cin >> res;
        switch (res){
            case 1:
                system("clear");
                cout << "______ 1. Negativo ______" <<endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        resultado=img.negativo(img);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        resultado=img.negativo(temporal);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            resultado=img.negativo(trabajo);        
                            optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 2: 
                system("clear");
                cout << "______ 1. Degradado Superior ______" <<endl;
                try{
                    
                    /* Nueva imagen*/
                    cout << "Crear nueva imagen" << endl;
                    img.crear();
                    resultado=img.degt(img);
                    //img.imprimir(resultado);
                    trabajo=optRes(resultado,img,trabajo,cargada);
                    break;
                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 3:
                system("clear");
                cout << "______ 3. Degradado inferior ______" <<endl;
                try{
                    
                    /* Nueva imagen*/
                    cout << "Crear nueva imagen" << endl;
                    img.crear();
                    resultado=img.degb(img);
                    //img.imprimir(resultado);
                    trabajo=optRes(resultado,img,trabajo,cargada);
                    break;
                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 4:
                system("clear");
                cout << "______ 4. Degradado Izq-Der______" <<endl;
                try{
                    
                    /* Nueva imagen*/
                    cout << "Crear nueva imagen" << endl;
                    img.crear();
                    resultado=img.degi(img);
                    //img.imprimir(resultado);
                    trabajo=optRes(resultado,img,trabajo,cargada);
                    break;
                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 5:
                system("clear");
                cout << "______ 5. Degradado Der-Iza ______" <<endl;
                try{
                    
                    /* Nueva imagen*/
                    cout << "Crear nueva imagen" << endl;
                    img.crear();
                    resultado=img.degd(img);
                    //img.imprimir(resultado);
                    trabajo=optRes(resultado,img,trabajo,cargada);
                    break;
                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 6:
                system("clear");
                cout << "______ Correcion Gamma______" <<endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.gamma(img,g);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.gamma(temporal,g);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            cout << "Ingrese el alpha:";
                            cin >> g;
                            resultado=img.gamma(trabajo,g);      
                            trabajo=optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            
            case 7:
                system("clear");
                cout << "____ Aclarado Logaritmico ____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.logaritmo(img,g);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.logaritmo(temporal,g);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            cout << "Ingrese el alpha:";
                            cin >> g;
                            resultado=img.logaritmo(temporal,g);      
                            trabajo=optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case  8:
                system("clear");
                cout << "____ Aclarado con Seno ____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        resultado=img.seno(img);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        resultado=img.seno(temporal);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            resultado=img.seno(trabajo);        
                            optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case  9:
                system("clear");
                cout << "____ Filtro  Exponencial____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.exponecial(img,g);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.exponecial(temporal,g);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            cout << "Ingrese el alpha:";
                            cin >> g;
                            resultado=img.exponecial(temporal,g);      
                            trabajo=optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 10:
                system("clear");
                cout << "____ Obscurecimiento  Exponencial____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.exponencial2(img,g);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        cout << "Ingrese el alpha:";
                        cin >> g;
                        resultado=img.exponencial2(temporal,g);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            cout << "Ingrese el alpha:";
                            cin >> g;
                            resultado=img.exponencial2(trabajo,g);      
                            trabajo=optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 11:
                system("clear");
                cout << "____ Aclarado con Coseno ____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        resultado=img.coseno(img);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        resultado=img.coseno(temporal);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            resultado=img.coseno(trabajo);        
                            optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 12:
                system("clear");
                cout << "____ Binarizar ____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        cout << "Ingrese el umbral:";
                        cin >> g;
                        resultado=img.binarizar(img,g);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        cout << "Ingrese el umbral:";
                        cin >> g;
                        resultado=img.binarizar(temporal,g);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            cout << "Ingrese el umbral:";
                            cin >> g;
                            resultado=img.binarizar(trabajo,g);        
                            optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            
            
            case 13:
                system("clear");
                cout << "____ Binarizar ____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        cout << "Ingrese el canal:";
                        cin >> canal;
                        resultado=img.binarizarCanal(img,canal);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        cout << "Ingrese el canal:";
                        cin >> canal;
                        resultado=img.binarizarCanal(temporal,canal);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            cout << "Ingrese el canal:";
                            cin >> canal;
                            resultado=img.binarizarCanal(trabajo,canal);        
                            optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 14:
                system("clear");
                cout << "____ Histograma ____" << endl;
                try{
                    op = img.respuesta();              
                    switch (op)
                    {
                    case '1':
                        /* Nueva imagen*/
                        cout << "Crear nueva imagen" << endl;
                        img.crear();
                        resultado=img.grayscale(img);
                        resultado=img.histogramaG(resultado);
                        //img.imprimir(resultado);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '2':
                        /* abrir imagen*/
                        cout << "Abrir imagen" << endl;
                        cout << "Ingrese el nombre de la imagen:";
                        cin >> nom;
                        temporal=img.abrir(nom,temporal);
                        temporal=img.grayscale(temporal);
                        resultado=img.histogramaG(temporal);
                        trabajo=optRes(resultado,img,trabajo,cargada);
                        break;
                    case '3':
                    //aplicar a cargada anterior mente 
                        if (cargada)
                        {
                            cout << "Aplicar a una carga anteriormente" << endl;
                            resultado=img.histogramaG(trabajo);        
                            optRes(resultado,img,trabajo,cargada);
                        }else{
                            img.reganar();
                        } 
                        break;
                    default:
                        cout << "opcion no contemplada" << endl;
                        break;
                    }

                }
                catch(exception& e)
                {
                    cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                }
                break;
            case 15:
                system("clear");
                    cout << "____ escala de grises ____" << endl;
                    try{
                        op = img.respuesta();              
                        switch (op)
                        {
                        case '1':
                            /* Nueva imagen*/
                            cout << "Crear nueva imagen" << endl;
                            img.crear();
                            resultado=img.grayscale(img);
                            trabajo=optRes(resultado,img,trabajo,cargada);
                            break;
                        case '2':
                            /* abrir imagen*/
                            cout << "Abrir imagen" << endl;
                            cout << "Ingrese el nombre de la imagen:";
                            cin >> nom;
                            temporal=img.abrir(nom,temporal);
                            resultado=img.grayscale(temporal);
                            trabajo=optRes(resultado,img,trabajo,cargada);
                            break;
                        case '3':
                        //aplicar a cargada anterior mente 
                            if (cargada)
                            {
                                cout << "Aplicar a una carga anteriormente" << endl;
                                temporal=img.grayscale(trabajo);
                                optRes(resultado,img,trabajo,cargada);
                            }else{
                                img.reganar();
                            } 
                            break;
                        default:
                            cout << "opcion no contemplada" << endl;
                            break;
                        }

                    }
                    catch(exception& e)
                    {
                        cerr << " ERROR No ingreso un entero"  << e.what() << '\n';
                    }
                    break;
            case 0:
                cout << "Cerrando programa...";
                system("clear");
                salir = true;
                //Sleep(8000);
                break;
            default:
                cout << "no exite la opcion..." << endl;
                break;
        }
    }
    
    return 0;
}
Img optRes( Img resultado, Img img,Img trabajo, bool cargada){
    string nom;
    char r;
    cout << "Filtro aplicado ..."<< endl;
    cout << "Seleccione una opcion:"<< endl;
    cout << "1. Guardar imagen"<< endl;
    cout << "2. Seguir aplicando filtros"<< endl;
    cout << "Respuesta:";
    cin >> r;
    switch (r){
        case '1':
            /* guardar */
            try
            {
                cout << endl << "Ingrese el nombre de la imagen:";
                cin >> nom;
                img.guardarN(resultado,nom);
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
            }
            
            break;
        case '2':
            // seguir aplicando filtros
            cargada = true;
            trabajo=img.copia(resultado);
        break;
        default:
        cout << "Ingreso una opcion no valida" << endl;
            break;
        }
    return trabajo;
}
