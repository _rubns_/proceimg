/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionAbrir;
    QAction *actionGuardar;
    QAction *actionNegativo;
    QAction *actionDegradados;
    QAction *actionCrear;
    QAction *actionSeno;
    QAction *actionEscala_de_Grises;
    QAction *actionEscala_de_grises;
    QAction *actionRGB;
    QAction *actionEscala_de_Grises_2;
    QAction *actionPor_canal;
    QAction *actionLog;
    QAction *actionExponecial;
    QAction *actionCoseno;
    QAction *actionExponecial_2;
    QAction *actionGamma;
    QWidget *centralwidget;
    QFrame *frame;
    QLabel *title_page;
    QLabel *imgView;
    QLabel *label;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuArchivo;
    QMenu *menuO_Punto;
    QMenu *menuAclarados;
    QMenu *menuBinarizar;
    QMenu *menuOscurecimiento;
    QMenu *menuO_Region;
    QMenu *menuHistograma;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(815, 411);
        actionAbrir = new QAction(MainWindow);
        actionAbrir->setObjectName(QString::fromUtf8("actionAbrir"));
        actionGuardar = new QAction(MainWindow);
        actionGuardar->setObjectName(QString::fromUtf8("actionGuardar"));
        actionNegativo = new QAction(MainWindow);
        actionNegativo->setObjectName(QString::fromUtf8("actionNegativo"));
        actionDegradados = new QAction(MainWindow);
        actionDegradados->setObjectName(QString::fromUtf8("actionDegradados"));
        actionCrear = new QAction(MainWindow);
        actionCrear->setObjectName(QString::fromUtf8("actionCrear"));
        actionSeno = new QAction(MainWindow);
        actionSeno->setObjectName(QString::fromUtf8("actionSeno"));
        actionEscala_de_Grises = new QAction(MainWindow);
        actionEscala_de_Grises->setObjectName(QString::fromUtf8("actionEscala_de_Grises"));
        actionEscala_de_grises = new QAction(MainWindow);
        actionEscala_de_grises->setObjectName(QString::fromUtf8("actionEscala_de_grises"));
        actionRGB = new QAction(MainWindow);
        actionRGB->setObjectName(QString::fromUtf8("actionRGB"));
        actionEscala_de_Grises_2 = new QAction(MainWindow);
        actionEscala_de_Grises_2->setObjectName(QString::fromUtf8("actionEscala_de_Grises_2"));
        actionPor_canal = new QAction(MainWindow);
        actionPor_canal->setObjectName(QString::fromUtf8("actionPor_canal"));
        actionLog = new QAction(MainWindow);
        actionLog->setObjectName(QString::fromUtf8("actionLog"));
        actionExponecial = new QAction(MainWindow);
        actionExponecial->setObjectName(QString::fromUtf8("actionExponecial"));
        actionCoseno = new QAction(MainWindow);
        actionCoseno->setObjectName(QString::fromUtf8("actionCoseno"));
        actionExponecial_2 = new QAction(MainWindow);
        actionExponecial_2->setObjectName(QString::fromUtf8("actionExponecial_2"));
        actionGamma = new QAction(MainWindow);
        actionGamma->setObjectName(QString::fromUtf8("actionGamma"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(0, 0, 821, 411));
        frame->setStyleSheet(QString::fromUtf8("QFrame{background-color:rgb(238, 238, 238);\n"
"}"));
        title_page = new QLabel(frame);
        title_page->setObjectName(QString::fromUtf8("title_page"));
        title_page->setGeometry(QRect(270, 70, 241, 161));
        QFont font;
        font.setFamily(QString::fromUtf8("icon-ui"));
        font.setPointSize(36);
        title_page->setFont(font);
        imgView = new QLabel(frame);
        imgView->setObjectName(QString::fromUtf8("imgView"));
        imgView->setGeometry(QRect(10, 50, 791, 331));
        imgView->setStyleSheet(QString::fromUtf8("QLabel{background-color:rgb(0, 0, 127);}"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(70, 10, 531, 31));
        QFont font1;
        font1.setFamily(QString::fromUtf8("MS Serif"));
        font1.setPointSize(16);
        label->setFont(font1);
        label->setWordWrap(true);
        MainWindow->setCentralWidget(centralwidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 815, 21));
        menuArchivo = new QMenu(menuBar);
        menuArchivo->setObjectName(QString::fromUtf8("menuArchivo"));
        menuO_Punto = new QMenu(menuBar);
        menuO_Punto->setObjectName(QString::fromUtf8("menuO_Punto"));
        menuAclarados = new QMenu(menuO_Punto);
        menuAclarados->setObjectName(QString::fromUtf8("menuAclarados"));
        menuBinarizar = new QMenu(menuO_Punto);
        menuBinarizar->setObjectName(QString::fromUtf8("menuBinarizar"));
        menuOscurecimiento = new QMenu(menuO_Punto);
        menuOscurecimiento->setObjectName(QString::fromUtf8("menuOscurecimiento"));
        menuO_Region = new QMenu(menuBar);
        menuO_Region->setObjectName(QString::fromUtf8("menuO_Region"));
        menuHistograma = new QMenu(menuBar);
        menuHistograma->setObjectName(QString::fromUtf8("menuHistograma"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuArchivo->menuAction());
        menuBar->addAction(menuO_Punto->menuAction());
        menuBar->addAction(menuO_Region->menuAction());
        menuBar->addAction(menuHistograma->menuAction());
        menuArchivo->addSeparator();
        menuArchivo->addAction(actionAbrir);
        menuArchivo->addSeparator();
        menuArchivo->addAction(actionGuardar);
        menuArchivo->addSeparator();
        menuArchivo->addAction(actionCrear);
        menuO_Punto->addAction(actionNegativo);
        menuO_Punto->addSeparator();
        menuO_Punto->addAction(actionDegradados);
        menuO_Punto->addSeparator();
        menuO_Punto->addAction(menuAclarados->menuAction());
        menuO_Punto->addSeparator();
        menuO_Punto->addAction(menuOscurecimiento->menuAction());
        menuO_Punto->addSeparator();
        menuO_Punto->addAction(actionEscala_de_Grises);
        menuO_Punto->addSeparator();
        menuO_Punto->addAction(menuBinarizar->menuAction());
        menuO_Punto->addSeparator();
        menuO_Punto->addAction(actionGamma);
        menuAclarados->addAction(actionSeno);
        menuAclarados->addSeparator();
        menuAclarados->addAction(actionLog);
        menuAclarados->addSeparator();
        menuAclarados->addAction(actionExponecial);
        menuAclarados->addSeparator();
        menuBinarizar->addAction(actionEscala_de_Grises_2);
        menuBinarizar->addSeparator();
        menuBinarizar->addAction(actionPor_canal);
        menuBinarizar->addSeparator();
        menuOscurecimiento->addAction(actionCoseno);
        menuOscurecimiento->addSeparator();
        menuOscurecimiento->addAction(actionExponecial_2);
        menuOscurecimiento->addSeparator();
        menuHistograma->addAction(actionEscala_de_grises);
        menuHistograma->addSeparator();
        menuHistograma->addAction(actionRGB);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Proc _rubn_", nullptr));
        actionAbrir->setText(QCoreApplication::translate("MainWindow", "Abrir", nullptr));
#if QT_CONFIG(shortcut)
        actionAbrir->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+A", nullptr));
#endif // QT_CONFIG(shortcut)
        actionGuardar->setText(QCoreApplication::translate("MainWindow", "Guardar", nullptr));
        actionNegativo->setText(QCoreApplication::translate("MainWindow", "Negativo", nullptr));
        actionDegradados->setText(QCoreApplication::translate("MainWindow", "Degradados", nullptr));
        actionCrear->setText(QCoreApplication::translate("MainWindow", "Crear", nullptr));
        actionSeno->setText(QCoreApplication::translate("MainWindow", "Seno", nullptr));
        actionEscala_de_Grises->setText(QCoreApplication::translate("MainWindow", "Escala de Grises", nullptr));
        actionEscala_de_grises->setText(QCoreApplication::translate("MainWindow", "Hist Grayscale", nullptr));
        actionRGB->setText(QCoreApplication::translate("MainWindow", "RGB", nullptr));
        actionEscala_de_Grises_2->setText(QCoreApplication::translate("MainWindow", "Escala de Grises", nullptr));
        actionPor_canal->setText(QCoreApplication::translate("MainWindow", "Por canal", nullptr));
        actionLog->setText(QCoreApplication::translate("MainWindow", "Log", nullptr));
        actionExponecial->setText(QCoreApplication::translate("MainWindow", "Exponecial", nullptr));
        actionCoseno->setText(QCoreApplication::translate("MainWindow", "Coseno", nullptr));
        actionExponecial_2->setText(QCoreApplication::translate("MainWindow", "Exponecial", nullptr));
        actionGamma->setText(QCoreApplication::translate("MainWindow", "Gamma", nullptr));
        title_page->setText(QCoreApplication::translate("MainWindow", "Bienvenido", nullptr));
        imgView->setText(QString());
        label->setText(QString());
        menuArchivo->setTitle(QCoreApplication::translate("MainWindow", "Archivo", nullptr));
        menuO_Punto->setTitle(QCoreApplication::translate("MainWindow", "O Punto", nullptr));
        menuAclarados->setTitle(QCoreApplication::translate("MainWindow", "Aclarados", nullptr));
        menuBinarizar->setTitle(QCoreApplication::translate("MainWindow", "Binarizar", nullptr));
        menuOscurecimiento->setTitle(QCoreApplication::translate("MainWindow", "Oscurecimiento", nullptr));
        menuO_Region->setTitle(QCoreApplication::translate("MainWindow", "O Region", nullptr));
        menuHistograma->setTitle(QCoreApplication::translate("MainWindow", "Histograma", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
