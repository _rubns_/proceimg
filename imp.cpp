#include <string>
#include <iostream>
#include "img.h"
#include <fstream>
#include<vector>
#include <math.h>
#include <limits>
#include <time.h>
#include <cmath>
//#include <windows.h>

using namespace std;


#define PI 3.14159265

void Img::crear(){
    try
    {
        string an;
        string al;
        string bp;
        string comm;
        cout << "Ingrese ancho:";
        cin >> an;
        cout << endl << "Ingrese alto:";
        cin >> al;
        cout << endl << "Ingrese profundidad:";
        cin >> bp;
        cout << endl << "Ingrese comentario:";
        cin >> comm;
        comentario = "#"+comm;
        ancho = stoi(an);
        alto = stoi(al);
        bpp = stoi(bp);
        temp = new Pixel[ancho*alto];
        int c = 0;
        for (int i = 0; i < alto; i++){
            for (int j = 0; j < ancho; j++){
                temp[c].R = 0;
                temp[c].G = 0;
                temp[c].B = 0;
                c++;
            }
        }
    }
    catch(const std::exception& e)
    {
        std::cerr <<  "Error al crear la imagen" << e.what() << '\n';
    }
}
Img Img::abrir(string s,  Img arc){
    string cadena;
    string reservadas [2] = {"P3","#"};
    int contador=0;
    int indice =0;
    vector<string> t;
    bool bandera = false;
    bool bp = false;
    bool r=false;
    bool g=false;
    bool b=false;
    try
    {
        ifstream archivo(s);
        if (archivo.is_open()){
            arc.nombre = s;
            while (getline (archivo, cadena, '\n'))
            {
                size_t found1 = cadena.find(reservadas[0]);
                size_t found2 = cadena.find(reservadas[1]);
                contador = 0;
                if (!bandera){
                    if ((found1 != string::npos)|| (found2 != string::npos) ){
                        if((found1 != string::npos)){
                            //cout << "formato" << cadena << endl;
                            arc.formato = cadena;
                        }else if((found2 != string::npos)){
                            //cout << "comentario" << cadena << endl;
                            arc.comentario += cadena;
                        }
                        bandera = false;
                    }else{
                        //cout << cadena << " no tiene una reservada" << endl;
                        contador =0;
                        for(int i=0; i<cadena.size(); i++){
                            if(i!=cadena.size()-1){
                                if(cadena[i]==' ' && i!=499 && cadena[i+1]!= ' '){
                                    contador++;
                                }
                            }
                        }
                        contador++;
                        if(contador >=2){
                            bandera = true;
                            char separador = ' ';
                            for(size_t p=0, q=0; p!=cadena.npos; p=q){
                                t.push_back(cadena.substr(p+(p!=0),(q=cadena.find(separador, p+1))-p-(p!=0)));
                            }
                            arc.ancho=stoi(t[0]);
                            arc.alto=stoi(t[1]); 
                            arc.temp = new Pixel[arc.ancho*arc.alto];
                            /* cout << "ancho" << arc.ancho << endl;
                            cout << "alto" << arc.alto << endl;
                            cout << "presione enter para continuar";
                            cin.ignore ( cin.rdbuf()->in_avail() ); 
                            cin.get(); */
                            bp=true;
                        }
                        /* cout << "presione enter tecla para continuar";    
                        cin.ignore ( cin.rdbuf()->in_avail() );
                        cin.get(); */ 
                    }

                }else {
                    if (bp)
                    {
                        bp =false;
                        /* cout << "el bpp es:"<< cadena << endl;
                        cin.ignore ( cin.rdbuf()->in_avail() );
                        cin.get(); */ 
                        arc.bpp=stoi(cadena);
                        r=true;
                        indice=0;
                    }
                    else
                        {
                            if(r){
                                arc.temp[indice].R=stoi(cadena);
                                r=false;
                                g=true;
                            }else if(g){
                                arc.temp[indice].G=stoi(cadena);
                                g=false;
                                b=true;
                            }else if(b){
                                arc.temp[indice].B=stoi(cadena);
                                b=false;
                                r=true;
                                indice++;
                            }
                        }

                }
            }
            archivo.close(); 
        }else
        {
            cout << "La imagen  no existe :,(" << endl;
            //Sleep(10000);
        }
    }
    catch(exception& e)
    {
        cerr << "Ocurrio un error al abrir el archivo " << e.what() << '\n';
    }
    return arc;
}
void Img::imprimir(Img im){
    cout << "Formtato" <<im.formato << endl;
    cout << "comentario" << im.comentario << endl;
    cout << "an al" << im.ancho << "  " << im.alto << endl;
    cout << "bpp" << im.bpp << endl;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             cout << "[" << im.temp[c].R << " " << im.temp[c].G << " " << im.temp[c].B << "] ";
             c++;
         }
         cout << endl;
     }
}
Img Img::negativo(Img im){
    Img copia;
    copia.temp= new Pixel [im.ancho* im.alto];
    copia.alto= im.alto;
    copia.ancho= im.ancho;
    copia.bpp= im.bpp;
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R= im.bpp - im.temp[c].R;
             copia.temp[c].G= im.bpp - im.temp[c].G;
             copia.temp[c].B= im.bpp - im.temp[c].B;
             c++;
         }
     }
    return copia;
}
void Img::guardar(Img im,string n){
    try{
        ofstream archivo("./img/"+n);
        archivo << im.formato << endl;
        archivo << im.comentario << endl;
        archivo << im.ancho << " " << im.alto << endl;
        archivo << im.bpp << endl;

        int c=0;
        for (int i = 0; i < im.alto; i++)
        {
            for (int j = 0; j < im.ancho; j++)
            {
                archivo << im.temp[c].R << endl;
                archivo << im.temp[c].G << endl;
                archivo << im.temp[c].B << endl;
                c++;
            }
        }
        archivo.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << "Ocurio un error al guardar" << '\n';
    }
}
Img Img::degt(Img im){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    float  proporcion = 0.0;
    proporcion = (float)im.alto/(float)im.bpp; 
    cout << proporcion << endl;
    int n=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             if((n+im.temp[c].R)*proporcion > 255)
                copia.temp[c].R=255;
             else
             {
                copia.temp[c].R=(n+im.temp[c].R)*proporcion;
             }
             if((n+im.temp[c].G)*proporcion > 255)
                copia.temp[c].G=255;
             else
             {
                copia.temp[c].G=(n+im.temp[c].G)*proporcion;
             }
             if((n+im.temp[c].B)*proporcion > 255)
                copia.temp[c].B=255;
             else
             {
                copia.temp[c].B=(n+im.temp[c].B)*proporcion;
             }
             c++;
         }
         n++;
     }
    return copia;
}
Img Img::degi(Img im){

    Img copia;
    copia.temp= new Pixel [im.ancho* im.alto];
    copia.alto= im.alto;
    copia.ancho= im.ancho;
    copia.comentario = im.comentario;
    copia.formato = im.formato;
    copia.bpp= im.bpp;  
    float  proporcion = 0.0;
    proporcion = (float)im.ancho/(float)im.bpp; 
    int n=0;
    for (int i = 0; i < im.ancho; i++)
     {
         for (int j = 0; j < im.alto; j++)
         {
             if((n+im.temp[(j*ancho)+i].R)*proporcion > 255)
                copia.temp[(j*ancho)+i].R=255;
             else
             {
                copia.temp[(j*ancho)+i].R=(n+im.temp[(j*ancho)+i].R)*proporcion;
             }
             if((n+im.temp[(j*ancho)+i].G)*proporcion > 255)
                copia.temp[(j*ancho)+i].G=255;
             else
             {
                copia.temp[(j*ancho)+i].G=(n+im.temp[(j*ancho)+i].G)*proporcion;
             }
             if((n+im.temp[(j*ancho)+i].B)*proporcion > 255)
                copia.temp[(j*ancho)+i].B=255;
             else
             {
                copia.temp[(j*ancho)+i].B=(n+im.temp[(j*ancho)+i].B)*proporcion;
             }
         }
         n++;
     }
    return copia;

}
Img Img::degb(Img im){
    Img copia;
    copia.temp= new Pixel [im.ancho* im.alto];
    copia.alto= im.alto;
    copia.ancho= im.ancho;
    copia.comentario = im.comentario;
    copia.formato = im.formato;
    copia.bpp= im.bpp;
    
    float  proporcion = 0.0;
    proporcion = (float)im.ancho/(float)im.bpp; 
    int n=0;
    for (int i =(im.alto-1) ; i >= 0 ; i--)
     {
         for (int j =(im.ancho-1); j >= 0; j--)
         {
             if((n+im.temp[(i*ancho)+j].R)*proporcion > 255)
                copia.temp[(i*ancho)+j].R=255;
             else
             {
                copia.temp[(i*ancho)+j].R=(n+im.temp[(i*ancho)+j].R)*proporcion;
             }
             if((n+im.temp[(i*ancho)+j].G)*proporcion > 255)
                copia.temp[(i*ancho)+j].G=255;
             else
             {
                copia.temp[(i*ancho)+j].G=(n+im.temp[(i*ancho)+j].G)*proporcion;
             }
             if((n+im.temp[(i*ancho)+j].B)*proporcion > 255)
                copia.temp[(i*ancho)+j].B=255;
             else
             {
                copia.temp[(i*ancho)+j].B=(n+im.temp[(i*ancho)+j].B)*proporcion;
             }
         }
         n++;
     }
    return copia;
}
Img Img::degd(Img im){
    Img copia;
    copia.temp= new Pixel [im.ancho* im.alto];
    copia.alto= im.alto;
    copia.ancho= im.ancho;
    copia.comentario = im.comentario;
    copia.bpp= im.bpp;
    copia.formato = im.formato;
    
    float  proporcion = 0.0;
    proporcion = (float)im.ancho/(float)im.bpp; 
    int n=0;
    for (int i =(im.ancho-1) ; i >= 0 ; i--)
     {
         for (int j =(im.alto-1); j >= 0; j--)
         {
             if((n+im.temp[(j*ancho)+i].R)*proporcion > 255)
                copia.temp[(j*ancho)+i].R=255;
             else
             {
                copia.temp[(j*ancho)+i].R=(n+im.temp[(j*ancho)+i].R)*proporcion;
             }
             if((n+im.temp[(j*ancho)+i].G)*proporcion > 255)
                copia.temp[(j*ancho)+i].G=255;
             else
             {
                copia.temp[(j*ancho)+i].G=(n+im.temp[(j*ancho)+i].G)*proporcion;
             }
             if((n+im.temp[(j*ancho)+i].B)*proporcion > 255)
                copia.temp[(j*ancho)+i].B=255;
             else
             {
                copia.temp[(j*ancho)+i].B=(n+im.temp[(j*ancho)+i].B)*proporcion;
             }
         }
         n++;
     }
    return copia;
}
char Img::respuesta(){
    char r;
    cout << "Aplicar Filtro a:"<< endl;
    cout << "1. Nueva imagen creada"<< endl;
    cout << "2. Abrir imagen"<< endl;
    cout << "3. Imagen utilizada en filtro anterior"<< endl;
    cout << "Respuesta:";
    cin >> r;
    /* cin.clear(); // unset failbit
    cin.ignore(numeric_limits<streamsize>::max(),'\n'); */
    return r;
}
char Img::preguntar(){
    char r;
    system("pause");
    cout << "Filtro aplicado.."<< endl;
    cout << "Seleccione una opcion:"<< endl;
    cout << "1. Guardar imagen"<< endl;
    cout << "2. Seguir aplicando filtros"<< endl;
    cout << "Respuesta:";
    cin >> r;
    /* cin.clear(); // unset failbit
    cin.ignore(numeric_limits<streamsize>::max(),'\n'); */
    return r;
}
Img Img::copia(Img im){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = im.temp[c].R;
             copia.temp[c].G = im.temp[c].G;
             copia.temp[c].B = im.temp[c].B;
             c++;
         }
     }
    return copia;
}
void Img::guardarN(Img im,string n){
    string comen;
    try
    {
    ofstream archivo("./img/"+n);
    archivo << "P3" << endl;
    cout << "Ingrese un comentario:";
    cin >>comen;
    archivo << "#"+comen << endl;
    archivo << im.comentario << endl;
    archivo << im.ancho << " " << im.alto << endl;
    archivo << im.bpp << endl;

    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             archivo << im.temp[c].R << endl;
             archivo << im.temp[c].G << endl;
             archivo << im.temp[c].B << endl;
             c++;
         }
     }
    archivo.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what( ) << "Ocurrio un error al guardar" << '\n';
    }
    
}
void Img::guardarT(Img im){
    string comen;
    try
    {
    ofstream archivo("./temp/tem.ppm");
    archivo << "P3" << endl;
    archivo << im.comentario << endl;
    archivo << im.ancho << " " << im.alto << endl;
    archivo << im.bpp << endl;

    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             archivo << im.temp[c].R << endl;
             archivo << im.temp[c].G << endl;
             archivo << im.temp[c].B << endl;
             c++;
         }
     }
    archivo.close();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what( ) << "Ocurrio un error al guardar" << '\n';
    }
    
}
Img Img::grayscale(Img im){

    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    int gray;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             gray = ((im.temp[c].R + im.temp[c].G + im.temp[c].B)/3);
             copia.temp[c].R = gray;
             copia.temp[c].G = gray;
             copia.temp[c].B = gray;
             c++;
         }
     }
    return copia;

}
Img Img::gamma(Img im , float g){
    
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.comentario = im.comentario;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = copia.bpp * (pow(((float)im.temp[c].R/copia.bpp),g));
             copia.temp[c].G = copia.bpp * (pow(((float)im.temp[c].G/copia.bpp),g));
             copia.temp[c].B = copia.bpp * (pow(((float)im.temp[c].B/copia.bpp),g));
             c++;
         }
     }
    return copia;
}
void Img::reganar(){
    cout << "No existe una cargada anteriormente " << endl;
    cout << "presione enter para continuar";
    cin.ignore ( cin.rdbuf()->in_avail() ); 
    cin.get();
    cin.ignore (256,'\n'); 
}
Img Img::logaritmo(Img im, float g){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = (copia.bpp / log (g*(im.bpp+1)))* log (g*(im.temp[c].R+1));
             copia.temp[c].G = (copia.bpp /log (g*(im.bpp+1)))* log (g*(im.temp[c].G+1));
             copia.temp[c].B = (copia.bpp / log (g*(im.bpp+1)))* log (g*(im.temp[c].B+1));
             c++;
         }
     }
    return copia;
}
Img Img::seno(Img im){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = copia.bpp * sin(((PI * im.temp[c].R)/(2*copia.bpp)));
             copia.temp[c].G = copia.bpp * sin(((PI * im.temp[c].G)/(2*copia.bpp)));
             copia.temp[c].B = copia.bpp * sin(((PI * im.temp[c].B)/(2*copia.bpp)));
             c++;
         }
     }
    return copia;
}
Img Img::exponecial(Img im, float g){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = (im.bpp/(1-(exp(((-1)*g)))))*(1-(exp(((-1*g)*im.temp[c].R)/im.bpp)));
             copia.temp[c].G = (im.bpp/(1-(exp(((-1)*g)))))*(1-(exp(((-1*g)*im.temp[c].G)/im.bpp)));
             copia.temp[c].B = (im.bpp/(1-(exp(((-1)*g)))))*(1-(exp(((-1*g)*im.temp[c].B)/im.bpp)));
             c++;
         }
     }
    return copia;
}
Img Img::coseno(Img im){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = im.bpp*(1-cos(((PI*im.temp[c].R)/(2*im.bpp))));
             copia.temp[c].G = im.bpp*(1-cos(((PI*im.temp[c].G)/(2*im.bpp))));
             copia.temp[c].B = im.bpp*(1-cos(((PI*im.temp[c].B)/(2*im.bpp))));
             c++;
         }
     }
    return copia;
}
Img Img::exponencial2(Img im, float g){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             copia.temp[c].R = (copia.bpp/(exp(g)-1))*(exp((g*im.temp[c].R)/im.bpp)-1);
             copia.temp[c].G = (copia.bpp/(exp(g)-1))*(exp((g*im.temp[c].G)/im.bpp)-1);
             copia.temp[c].B = (copia.bpp/(exp(g)-1))*(exp((g*im.temp[c].B)/im.bpp)-1);
             c++;
         }
     }
    return copia;
}
Img Img::histogramaG(Img im){
    Img copia;
    //alto por ancho
    copia.temp= new Pixel [256*100];
    copia.alto= 100;
    copia.ancho= 256;
    copia.bpp=256;
    float histogram[256]={0.0},histogramN[256]={0.0},normal=0.0;
    int c=0,porcentajeOri,nup,mayor=0;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++){
             histogram[im.temp[(i*im.ancho)+j].R]++; 
             if(histogram[im.temp[(i*im.ancho)+j].R] > mayor){
                 mayor=im.temp[(i*im.ancho)+j].R;
             }      
         }
     }
    porcentajeOri=im.alto*im.ancho; 
    //cout <<"Posicion:"<< mayor <<":"<<histogram[mayor] << endl;
    
     for(int i=0; i<256; i++){
         //cout <<i+1<<":"<<histogram[i] << endl;
        histogramN[i]=(histogram[i]*100)/porcentajeOri;
        normal=normal+histogramN[i];
     } 
     /* for(int i=0; i<256; i++){
        cout <<i+1<<":"<<histogramN[i] << endl;
     }  */
     cout <<"Porcentaje de la imagen procesada:"<<normal<<endl;
     for (int i =0 ; i < 256; i++){
        if(histogramN[i] > 1.0){
            nup=floor(histogramN[i]);
            for (int j =99; j >=0; j--){
                if (nup!=0){
                        copia.temp[(j*256)+i].R=254;
                        copia.temp[(j*256)+i].G=33;
                        copia.temp[(j*256)+i].B=255;
                        nup--;
                    }
                }
            //cout <<i+1<<":"<<histogramN[i] << endl;
            }
     }

    return copia;
}
Img Img::binarizar(Img im ,float g){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0;
    int umbral=128;
    if(g > 10.0){
        g=g;
    }else
    {
        g=128;
    }
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             if(im.temp[c].R>g){
                 copia.temp[c].R=255;
                 copia.temp[c].G=255;
                 copia.temp[c].B=255;
             }else{
                 copia.temp[c].R=0;
                 copia.temp[c].G=0;
                 copia.temp[c].B=0;
             }
             c++;
         }
     }
    return copia;
}
Img Img::binarizarCanal(Img im ,string canal){
    Img copia;
    copia.ancho = im.ancho;
    copia.alto = im.alto ;
    copia.bpp = im.bpp;
    copia.temp = new Pixel[copia.ancho*copia.alto];
    copia.comentario = im.comentario;
    int c=0, gray;
    int umbral=179;
    for (int i = 0; i < im.alto; i++)
     {
         for (int j = 0; j < im.ancho; j++)
         {
             if(im.temp[c].R==umbral){
                 copia.temp[c].R=im.temp[c].R;
                 copia.temp[c].G=im.temp[c].G;
                 copia.temp[c].B=im.temp[c].B;
             }else{
                 gray = ((im.temp[c].R + im.temp[c].G + im.temp[c].B)/3);
                copia.temp[c].R = gray;
                copia.temp[c].G = gray;
                copia.temp[c].B = gray;
             }
             c++;
         }
     }
    return copia;
}