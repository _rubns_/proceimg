#ifndef IMG_H
#define IMG_H
#include <string>
namespace std
{
    class Pixel
    {
        public:
            int R;
            int G;
            int B;
            Pixel() { R = 0; G = 0; B = 0; };
            ~Pixel() { };
    };
    class Img: public Pixel{

        public:
            int ancho;
            int alto;
            int bpp;
            string nombre;
            string formato;
            string comentario;
            Pixel* p;
            Pixel* temp;
            void crear();
            void crear2(int,int,int);
            Img abrir(string,Img);
            void guardar(Img, string);
            void imprimir(Img);
            Img negativo(Img);
            Img degt(Img);
            Img degb(Img);
            Img degi(Img);
            Img degd(Img);
            char preguntar();
            char respuesta();
            void guardarN(Img, string,string);
            void guardarT(Img);
            Img gamma(Img, float);
            Img grayscale(Img);
            Img copia(Img);
            void reganar();
            Img logaritmo(Img,float);
            Img seno(Img);
            Img exponecial(Img, float);
            Img exponencial2(Img, float);
            Img coseno(Img);
            Img binarizar(Img, float);
            Img binarizarCanal(Img, string);
            Img histogramaG(Img);
            Img histogramaRGB(Img,string);
            Img bordeh(Img);
            Img bordev(Img);
            Img contornol1(Img);
        };
}


#endif // IMG_H
