#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <form.h>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void printImg(QString);
    void messageFiltro(QString);
    ~MainWindow();

private slots:
    void on_actionAbrir_triggered();

    void on_actionNegativo_triggered();

    void on_actionSeno_triggered();

    void on_actionLog_triggered();

    void on_actionExponecial_triggered();

    void on_actionEscala_de_Grises_triggered();

    void on_actionCoseno_triggered();

    void on_actionExponecial_2_triggered();

    void on_actionEscala_de_Grises_2_triggered();

    void on_actionPor_canal_triggered();

    void on_actionRGB_triggered();

    void on_actionEscala_de_grises_triggered();

    void on_actionGuardar_triggered();

    void on_actionHorizontal_triggered();

    void on_actionVertical_triggered();

    void on_actionContorno_L1_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
