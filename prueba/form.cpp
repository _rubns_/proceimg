#include "form.h"
#include "ui_form.h"
#include <QMessageBox>

Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
}

Form::~Form()
{
    delete ui;
}
void Form::printImg(QString archivo){
    QImage img(archivo);
    if(img.isNull()){
        QMessageBox::critical(this, tr("Error"),tr("no se puede abrir la imagen :,C").arg(archivo));
        return;
    }else{
        ui->label->setPixmap(QPixmap::fromImage(img));
        ui->label->setScaledContents(true);
        ui->label->show();
    }
}
void Form::messageFiltro(QString mss){
    ui->label_2->setText(mss);
    ui->label_2->show();
}
