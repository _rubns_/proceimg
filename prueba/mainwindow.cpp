#include "mainwindow.h"
#include "img.h"
#include <string>
#include "ui_mainwindow.h"
#include  <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>
#include <iostream>
#include <QtWidgets>

//variables globales
std::Img im,anterior,resultado,temporal,histograma;
bool cargada=false;
QString archivo="..\\img\\temp.ppm";

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->imgView->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionAbrir_triggered()
{
    std::string arc;
    QString archivo=QFileDialog::getOpenFileName(this,tr("Abrir Imagen"),QDir::currentPath(),tr("imagenes(*.ppm)"));
    if(archivo!=""){
      QMessageBox::information(this,tr("Nombre de la imagen"),archivo);
      QImage img(archivo);
      if(img.isNull()){
          QMessageBox::information(this, tr("Error"),tr("no se puede abrir la imagen :,C").arg(archivo));
          return;
      }else{
          arc=archivo.toStdString();
          temporal=im.abrir(arc,temporal);
          cargada=true;
          ui->imgView->setPixmap(QPixmap::fromImage(img));
          ui->imgView->setScaledContents(true);
          ui->imgView->show();
          ui->title_page->hide();
      }
    }
}

void MainWindow::printImg(QString archivo){
    QImage img(archivo);
    if(img.isNull()){
        QMessageBox::critical(this, tr("Error"),tr("no se puede abrir la imagen :,C").arg(archivo));
        return;
    }else{
        cargada=true;
        ui->imgView->setPixmap(QPixmap::fromImage(img));
        ui->imgView->setScaledContents(true);
        ui->imgView->show();
        ui->title_page->hide();
    }
}


void MainWindow::on_actionNegativo_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.negativo(temporal);
        temporal=resultado;
        im.guardarN(resultado,"temp.ppm","Negativo");
        messageFiltro("Filtro aplicado:Negativo");
        printImg(archivo);
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }

}
void MainWindow::messageFiltro(QString mss){
    ui->label->setText(mss);
    ui->label->show();
}
void MainWindow::on_actionSeno_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.seno(temporal);
        temporal=resultado;
        im.guardarN(resultado,"temp.ppm","Seno");
        messageFiltro("Filtro aplicado:Seno");
        printImg(archivo);
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionLog_triggered()
{
    if(cargada){
        anterior=temporal;
        bool ok;
        double alpha = QInputDialog::getDouble(this,tr("Ingrese valor"),tr("ingrese alpha:"),1,1,255,1,&ok);
        if(ok && alpha>= 1.0){
            resultado=im.logaritmo(temporal,alpha);
            temporal=resultado;
            std::string filtro = "Filtro aplicado:Logaritmo a "+std::to_string(alpha);
            messageFiltro(QString::fromStdString(filtro));
            im.guardarN(resultado,"temp.ppm",filtro);

            printImg(archivo);
        }

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionExponecial_triggered()
{
    if(cargada){
        anterior=temporal;
        bool ok;
        double alpha = QInputDialog::getDouble(this,tr("Ingrese valor"),tr("ingrese alpha:"),1,1,255,1,&ok);
        if(ok && alpha>= 1.0){
            resultado=im.exponecial(temporal,alpha);
            temporal=resultado;
            std::string filtro = "Filtro aplicado :Exponencial a "+std::to_string(alpha);
            messageFiltro(QString::fromStdString(filtro));
            im.guardarN(resultado,"temp.ppm",filtro);

            printImg(archivo);
        }else{
            QMessageBox::critical(this,tr("Error"),tr("No ingreso un alpha valido"));
        }

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionEscala_de_Grises_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.grayscale(temporal);
        temporal=resultado;
        std::string filtro = "Filtro aplicado:Escala de Grises";
        messageFiltro(QString::fromStdString(filtro));
        im.guardarN(resultado,"temp.ppm",filtro);

        printImg(archivo);

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionCoseno_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.coseno(temporal);
        temporal=resultado;
        im.guardarN(resultado,"temp.ppm","Coseno");
        messageFiltro("Filtro aplicado:Coseno");

        printImg(archivo);
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionExponecial_2_triggered()
{
    if(cargada){
        anterior=temporal;
        bool ok;
        double alpha = QInputDialog::getDouble(this,tr("Ingrese valor"),tr("ingrese alpha:"),1,1,255,1,&ok);
        if(ok && alpha>= 1.0){
            resultado=im.exponencial2(temporal,alpha);
            temporal=resultado;
            std::string filtro = "Filtro aplicado :Exponencial a "+std::to_string(alpha);
            messageFiltro(QString::fromStdString(filtro));
            im.guardarN(resultado,"temp.ppm",filtro);

            printImg(archivo);
        }
        else{
                    QMessageBox::critical(this,tr("Error"),tr("No ingreso un alpha valido"));
                }

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionEscala_de_Grises_2_triggered()
{
    if(cargada){
        anterior=temporal;
        bool ok;
        int alpha = QInputDialog::getInt(this,tr("Ingrese valor"),tr("ingrese umbral:"),1,1,255,1,&ok);
        if(ok && alpha>= 1.0){
            resultado=im.binarizar(temporal,alpha);
            temporal=resultado;
            std::string filtro = "Filtro aplicado :Binarizar por umbral adaptativo "+std::to_string(alpha);
            messageFiltro(QString::fromStdString(filtro));
            im.guardarN(resultado,"temp.ppm",filtro);
            printImg(archivo);
        }else{
            QMessageBox::critical(this,tr("Error"),tr("No ingreso un alpha valido"));
        }

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionPor_canal_triggered()
{
    if(cargada){
        anterior=temporal;
        bool ok;
        QString canal = QInputDialog::getText(this, tr("Ingrese canal"), tr("ingrese canal para Binarizar \n R:Red \n G:Green \n B:Blue"), QLineEdit::Normal, "Ingrese Canal", &ok);
        if(ok && ((canal == "R" || canal == "G" || canal == "B" ))){
            resultado=im.binarizarCanal(temporal,canal.toStdString());
            temporal=resultado;
            std::string filtro = "Filtro aplicado :Binarizado por canal"+canal.toStdString();
            messageFiltro(QString::fromStdString(filtro));
            im.guardarN(resultado,"temp.ppm",filtro);
            printImg(archivo);
        }else{
            QMessageBox::critical(this,tr("Error"),tr("No ingreso elformato de canala o el umbral no esta permitido. "));
        }

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionRGB_triggered()
{
    if(cargada){
        anterior=temporal;
        std::string color[3]={"R","G","B"};
        for (int i=0;i<3;i++) {
            histograma=im.histogramaRGB(temporal,color[i]);
            QString archivo=".\\temp\\histo.ppm";
            im.guardarT(histograma);
            Form *histograma1 = new Form;
            histograma1->printImg(archivo);
            histograma1->messageFiltro(QString::fromStdString("histograma canal:"+color[i]));
            histograma1->show();
        }

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionEscala_de_grises_triggered()
{
    if(cargada){
        resultado=im.grayscale(temporal);
        histograma=im.histogramaG(resultado);
        QString archivo=".\\temp\\histo.ppm";
        im.guardarT(histograma);
        im.guardarN(resultado,"gray.ppm","filtro de escala de grises para histograma");
        printImg(".\\img\\gray.ppm");
        messageFiltro("filtro de escala de grises para histograma");
        Form *histograma1 = new Form;
        histograma1->printImg(archivo);
        histograma1->messageFiltro("histograma de escala de grises");
        histograma1->show();
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionGuardar_triggered()
{
    if(cargada){
        QString imgname = QFileDialog::getSaveFileName(this,tr("Guardar Imagen"),"/home",tr("Imagen (*.ppm)"));
        im.guardar(temporal,imgname.toStdString());

    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para guardar"));
    }
}

void MainWindow::on_actionHorizontal_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.bordeh(temporal);
        temporal=resultado;
        im.guardarN(resultado,"temp.ppm","Detecion de Borde Horizontal");
        messageFiltro("Filtro aplicado:Deteccion de bordes Horizontal");
        printImg(archivo);
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionVertical_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.bordev(temporal);
        temporal=resultado;
        im.guardarN(resultado,"temp.ppm","Detecion de Borde Vertical");
        messageFiltro("Filtro aplicado:Deteccion de bordes Vertical");
        printImg(archivo);
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}

void MainWindow::on_actionContorno_L1_triggered()
{
    if(cargada){
        anterior=temporal;
        resultado=im.contornol1(temporal);
        temporal=resultado;
        im.guardarN(resultado,"temp.ppm","Detecion de Contorno L1");
        messageFiltro("Filtro aplicado:Deteccion de Contorno L1");
        printImg(archivo);
    }else{
        QMessageBox::critical(this,tr("Error"),tr("No hay imagen cargada para aplicar filtro"));
    }
}
